var types = [
{
  "name": "heat",
  "image": "assets/fire.png",
  "available": true
},
{
  "name": "water",
  "image": "assets/water.png",
  "available": true
},
{
  "name": "life",
  "image": "assets/life.png",
  "available": true
},
{
  "name": "hammer",
  "image": "assets/hammer.png",
  "available": true
},
{
  "name": "dirt",
  "image": "assets/dirt_littlePizza.png",
  "available": true
},
{
  "name": "mud",
  "image": "assets/mud_littlePizza.png",
  "available": false
},
{
  "name": "grain",
  "image": "assets/grain.png",
  "available": false
},
{
  "name": "livestock",
  "image": "assets/livestock.png",
  "available": false
},
{
  "name": "metal",
  "image": "assets/metal.png",
  "available": false
},
{
  "name": "bucket",
  "image": "assets/bucket.png",
  "available": false
},
{
  "name": "flour",
  "image": "assets/flour.png",
  "available": false
},
{
  "name": "dough",
  "image": "assets/dough.png",
  "available": false
},
{
  "name": "meat",
  "image": "assets/meat.png",
  "available": false
},
{
  "name": "milk",
  "image": "assets/milk.png",
  "available": false
},
{
  "name": "bread",
  "image": "assets/bread.png",
  "available": false
},
{
  "name": "cheese",
  "image": "assets/cheese.png",
  "available": false
},
{
  "name": "tomato",
  "image": "assets/tomato.png",
  "available": false
},
{
  "name": "tomato sauce",
  "image": "assets/tomato_sauce.png",
  "available": false
},
{
  "name": "pizza dough",
  "image": "assets/pizza_dough.png",
  "available": false
},
{
  "name": "red sauce dough",
  "image": "assets/pizza_redsauce.png",
  "available": false
},
{
  "name": "cheesy dough",
  "image": "assets/pizza_withcheese.png",
  "available": false
},
{
  "name": "uncooked pizza",
  "image": "assets/ready_to_bake.png",
  "available": false
},
{
  "name": "pizza",
  "image": "assets/pizza.png",
  "available": false
},
{
  "name": "pig",
  "image": "assets/pig_littlePizza.png",
  "available": false
},
{
  "name": "ham",
  "image": "assets/ham_littlePizza.png",
  "available": false
},
{
  "name": "pineapple",
  "image": "assets/badPineapple_littlePizza.png",
  "available": false
},
{
  "name": "hawaiian pizza",
  "image": "assets/hawaiianStyle_littlePizza.png",
  "available": false
},
{
  "name": "deep dish crust",
  "image": "assets/deepDishCrust_littlePizza.png",
  "available": false
}
];

define([], function() {
    return types;
});