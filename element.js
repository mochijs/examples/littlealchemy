// export element class.
define(['../engine/Mochi'], function (Mochi) {


    /**
     * Creates a new recipe. Allows for producing new elements.
     * @param type1 The first type that will be combined.
     * @param type2 The second type that will be combined.
     * @param type3 The third type that could be combined.
     * @param result The type produced.
     * @constructor
     */
    var Recipe = function(type1, type2, type3, result) {
        this.type1 = type1;
        this.type2 = type2;
        this.type3 = type3;
        this.result = result;
    };

    /** Checks to see if the given elements match a recipe */
    Recipe.prototype.check = function(element1, element2, element3) {
        // TODO Check if we need to handle reverses as well, although I think they are separate recipes.
        return element1.type === this.type1 && element2.type === this.type2 && element3.type === this.type3;
    };


    /**
     * Describes the element type, its combinations, and whether or not
     * its been discovered. Allows for shared data between element instances.
     * @param name The name of the element type
     * @param sprite The sprite that the elements will use for appearance
     * @param available (default false) whether or not the element is available currently
     * @constructor
     */
    class ElementType {

        constructor(data) {
            /** The name of the element (e.g. plant) */
            this.name = data.name;
            /** The sprite object that the element will appear as */
            this.sprite = data.sprite;
            /** Whether or not this element type is available (has been discovered or initially allowed */

            this.available = (data.available === undefined) ? false : data.available;
        }
        /** Called when this element type is discovered in a combo */
        discover() {
            this.available = true;
        }
    }

    /**
     * A list that maintains a list of element types and can render them.
     * Also maintains what combinations are allowed between these elements.
     * Takes up entire height, variable x position.
     * @constructor
     */
    var ElementList = function(x, maxY) {
        this.types = [];
        this.recipes = [];
        this.x = x;
        this.y = 0;
        this.maxY = maxY;
    };
    /** Add an element type to this list */
    ElementList.prototype.add = function(type) {
        this.types.push(type);
    };
    /** Add a new elemental formula */
    ElementList.prototype.addRecipe = function(recipe) {
        this.recipes.push(recipe);
    };
    /**
     * Adds a recipe based on the provided element names.
     * @param type1Name
     * @param type2Name
     * @param type3Name
     * @param resultName
     * @returns {boolean} True if the recipe is added, false if not.
     */
    ElementList.prototype.addRecipeByName = function(type1Name, type2Name, type3Name, resultName) {
        // Make sure all provided types have a name
        var type1 = this._getTypeByName(type1Name);
        var type2 = this._getTypeByName(type2Name);
        var type3 = null;
        if(type3Name !== null)
            var type3 = this._getTypeByName(type3Name);
        var result = this._getTypeByName(resultName);
        if (type1 === null || type2 === null || result === null)
            return false;
        else if(type3 === null)
            this.addRecipe(new Recipe(type1, type2, null, result));
        else
            this.addRecipe(new Recipe(type1, type2, type3, result));
        return true;
    };
    ElementList.prototype.addReversibleRecipeByName = function(type1Name, type2Name, type3Name, resultName) {
        this.addRecipeByName(type1Name, type2Name, type3Name, resultName);
        this.addRecipeByName(type2Name, type1Name, type3Name, resultName);
        this.addRecipeByName(type3Name, type1Name, type2Name, resultName);
        this.addRecipeByName(type1Name, type3Name, type2Name, resultName);
        this.addRecipeByName(type2Name, type3Name, type1Name, resultName);
        this.addRecipeByName(type3Name, type2Name, type1Name, resultName);
    };
    /** If there is a recipe for the given elements, returns result type. */
    ElementList.prototype.getRecipeFor = function(element1, element2, element3) {
        if(element3 === null){
            for (var i = 0; i < this.recipes.length; i++) {
                if (this.recipes[i].type1 === element1.type && this.recipes[i].type2 === element2.type && this.recipes[i].type3 === null)
                    return this.recipes[i].result;
            }
        }
        else {
            for (var i = 0; i < this.recipes.length; i++) {
                if (this.recipes[i].type1 === element1.type && this.recipes[i].type2 === element2.type && this.recipes[i].type3 === element3.type)
                    return this.recipes[i].result;
            }
        }
        return null;
    };

    /** @returns The type with a matching name or null if none found */
    ElementList.prototype._getTypeByName = function(name) {
        for (var i = 0; i < this.types.length; i++) {
            if (this.types[i].name === name)
                return this.types[i];
        }
        return null;
    };
    /** Coordinates are generated, inefficient */
    ElementList.prototype._getTypeCoords = function(index) {
        if (!this.types[index].available)
            return false;
        var y = this.y;
        var distanceY = 10;
        for (var i = 0; i <= index; i++) {
            if (!this.types[i].available)
                continue;
            if (i !== 0) {
                y += distanceY;
                y += this.types[i - 1].sprite.height;
            }
        }
        return this._handleYOverflow(index, { x: this.x, y: y });
    };
    /** Handles if the list overflows off the page */
    ElementList.prototype._handleYOverflow = function(index, coords) {
        // if the element is off the page, they are given an additional x and their y is reset.
        var totalY = coords.y + this.types[index].sprite.height;
        if (totalY < this.maxY)
            return coords;
        // we need to handle the overflow
        var distanceX = this.types[index].sprite.width + 60;
        var xMultiplies = Math.floor(totalY / this.maxY);
        var remainderY = totalY % this.maxY;
        var newX = distanceX * xMultiplies;
        return {x: newX, y: remainderY};
    };
    /** Returns the element in the list at the coordinates, or null if none found */
    ElementList.prototype.getTypeAtCoords = function(x, y) {
        // Check all of our types to see if one is under the selected point.
        for (var i = 0; i < this.types.length; i++) {
            var coords = this._getTypeCoords(i);
            if (coords && Mochi.Math.containsPoint(x, y, coords.x, coords.y,
                    this.types[i].sprite.width, this.types[i].sprite.height)) {
                return this.types[i];
            }
        }
        return null;
    };
    /** Returns the type under the element if there is one */
    ElementList.prototype.getTypeUnderElement = function(element) {
        var elementBox = {
            x: element.x,
            y: element.y,
            width: element.type.sprite.width,
            height: element.type.sprite.height
        };
        for (var i = 0; i < this.types.length; i++) {
            var box = this._getTypeCoords(i);
            if (box === null)
                continue;
            box.width = this.types[i].sprite.width;
            box.height = this.types[i].sprite.height;
            if (Mochi.Math.containsRect(elementBox, box)) {
                return this.types[i];
            }
        }
        return null;
    };
    /** Draws the element list with all its available types */
    ElementList.prototype.draw = function(ctx) {
        // Each element gets a different position, incrementing over time.
        var textDistanceX = 10;
        for (var i = 0; i < this.types.length; i++) {
            if (!this.types[i].available)
                continue;
            var coords = this._getTypeCoords(i);
            this.types[i].sprite.draw(ctx, coords.x, coords.y);
            // also render the name of the element next to it
            var textX = coords.x + this.types[i].sprite.width + textDistanceX;
            var textY = coords.y + this.types[i].sprite.height / 2;
            ctx.font = "16px Tahoma";
            ctx.textBaseline = 'middle';
            ctx.textAlign = 'start';
            ctx.fillText(this.types[i].name, textX, textY);
        }
    };


    /**
     * The main game object, has a type and can be combined.
     * @constructor
     */
    class Element extends Mochi.Entity {
        constructor(type, x, y){
            super();
            this.type = type;
            this.setCenter(x, y);
        }
        /** Changes the center of the element to be the given coordinate */
        setCenter(x, y) {
            this.x = x - this.type.sprite.width / 2;
            this.y = y - this.type.sprite.height / 2;
        };
        /** Returns true if this element contains the given point */
        containsPoint(x, y) {
            return Mochi.Math.containsPoint(x, y, this.x, this.y, this.type.sprite.width, this.type.sprite.height);
        };
        draw(ctx){
            this.type.sprite.draw(ctx, this.x, this.y);
            let textX = this.x + this.type.sprite.width / 2;
            let textY = this.y + this.type.sprite.height;
            ctx.font = "12px Tahoma";
            ctx.textBaseline = 'top';
            ctx.textAlign = 'center';
            ctx.fillText(this.type.name, textX, textY);

        }
    }
    Element.Name = 'element';

    return {
        Element: Element,
        ElementType: ElementType,
        ElementList: ElementList,
        Recipe: Recipe
    };
});
