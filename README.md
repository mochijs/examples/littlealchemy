# Team:
 1. Kenny Jones (kgjones3)
 2. Justin Patzer (jepatzer)
 3. Robby Seligson (rfseligs)

# For the Professor:

Everything is packaged inside. Just open up index.html, which uses requirejs to
load main.js. 

We did a food-based game! The goal of this small rendition is to try to make pizza.
We added hawaiian pizza to show off triple combinations.

It isn't easy!

## Engine Integration
The game shows off our Sprite class as well as our asynchronous asset management system.

Additionally, we've created convenient input handlers for mouse events and keyboard events.
In the future, these will be added to our engine's event system for seamless integration.

The game class is also part of the engine, and it handles creating the canvas and running
the main loop. It allows for easy initial creation of games without adding infrastructure
usually necessary, and it updates on a consistent timestep.

Lastly, we have a small math library that the game takes advantage of for collision.

## Note about Assets

Assets are partially created by the team and partially from free domain icon packages.

# Checking out project
This project contains a 'submodule' (a git repository in a git repository).

Instead of just cloning it directly, clone it recursively.

```git
git clone -b {yourbranch} --recursive git@gitlab.com:csc481/littlealchemy.git
```

If you already downloaded the repo, you can type.

```git
git submodule init
git submodule update
```

It'll pull the game engine in.
